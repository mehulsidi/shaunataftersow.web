﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Common
{
    public class ViewHelper
    {
        public const string Home = "~/Views/Home/Index.cshtml";

        public const string Organization = "~/Views/Organization/Index.cshtml";
        public const string AddEditOrganization = "~/Views/Organization/AddEditOrganization.cshtml";

        public const string Control = "~/Views/Control/Index.cshtml";
        public const string AddEditControl = "~/Views/Control/AddEditControl.cshtml";

        public const string Hazard = "~/Views/Hazard/Index.cshtml";
        public const string AddEditHazard = "~/Views/Hazard/AddEditHazard.cshtml";

        public const string Task = "~/Views/Task/Index.cshtml";
        public const string AddEditTask = "~/Views/Task/AddEditTask.cshtml";

        public const string Activity = "~/Views/Activity/Index.cshtml";
        public const string AddEditActivity = "~/Views/Activity/AddEditActivity.cshtml";

        public const string User = "~/Views/User/Index.cshtml";
        public const string AddEditUser = "~/Views/User/AddEditUser.cshtml";

        public const string UserOrganisation = "~/Views/UserOrganisation/Index.cshtml";
        public const string AddEditUserOrganisation = "~/Views/UserOrganisation/AddEditUserOrganisation.cshtml";
    }
}
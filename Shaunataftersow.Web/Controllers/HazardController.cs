﻿using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;
using Shaunataftersow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Shaunataftersow.Web.Controllers
{
    public class HazardController : Controller
    {
        public ActionResult Index()
        {
            return View(ViewHelper.Hazard);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            ReadControlList();
            HazardModel objModel = new HazardModel();
            objModel.Icon = "icon.png";

            if (Id > 0)
            {
                var result = new Hazard();
                var uri = "Hazards/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                if (result != null) {
                    objModel.Description = result.Description;
                    objModel.Type = result.Type;
                    objModel.Icon= result.Icon;
                    objModel.ControlIds = GetHazardControl(Id);
                    objModel.id = Id;
                }
            }
            
            return View(ViewHelper.AddEditHazard, objModel);
        }
        public async Task<ActionResult> SaveHazard(HttpPostedFileBase file, HazardModel model)
        {
            string Flag = string.Empty;
            if (file != null)
            {
                string path = Server.MapPath(WebConfigurationManager.AppSettings["HazardPath"]);
                bool folderExists = System.IO.Directory.Exists(path);
                if (!folderExists)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string FileName = DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmssfff") + file.FileName.Split('.')[0].ToString() + fileExtension;
                FileName = FileName.Replace(" ", "_");
                file.SaveAs(path + FileName);
                model.Icon = string.IsNullOrEmpty(FileName) ? "" : FileName;
            }
            


            Hazard objHazard = new Hazard();
            objHazard.Description = model.Description;
            objHazard.Type = model.Type;
            objHazard.Icon = model.Icon;
            objHazard.Controls = new List<HazardControl>();
            if (!string.IsNullOrEmpty(model.ControlIds))
            {
                foreach (var item in model.ControlIds.Split(',')) {
                    HazardControl obj = new HazardControl();
                    obj.ControlId = Convert.ToInt32(item);
                    objHazard.Controls.Add(obj);
                }
            }

            if (model.id == 0)
            {
                Flag = await WebApiHelper.HttpClientPostPassEntityReturnString<Hazard>(objHazard, "Hazards");
            }
            else
            {
                objHazard.Id = model.id;
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<Hazard>(objHazard, "Hazards/" + model.id);
            }
            //return RedirectToAction("Index", HazardlerHelper.Hazard);
            return RedirectToAction("Index", ControllerHelper.Hazard);
        }
        public ActionResult DeleteHazard(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("Hazards/" + id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> ReadHazardList()
        {
            var result = new List<HazardModel>();
            var uri = "Hazards";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.Hazard);
            }
        }
        public void ReadControlList()
        {
            var result = new List<ControlModel>();
            var uri = "Controls";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            IEnumerable<SelectListItem> items = result
           .Select(c => new SelectListItem
           {
               Value = c.id.ToString(),
               Text = c.Description
           });
            ViewBag.ControlList = items;
        }
        public string GetHazardControl(int HazardId) {
            string controlsId = string.Empty;
            var result = new List<HazardControl>();
            var uri = "HazardControls";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            controlsId = string.Join(",", result.Where(x => x.HazardId == HazardId).Select(x => x.ControlId));
            return controlsId;
        } 
    }
}
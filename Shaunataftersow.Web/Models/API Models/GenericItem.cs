﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Shaunataftersow.Models
{
   public class GenericItem:EntityWithDate
    {
        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(255)]
        public string Type { get; set; }

        public string Icon { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Shaunataftersow.Models
{
    public class HazardControl : EntityWithDate
    {
        [ForeignKey(nameof(Hazard))]
        public int HazardId { get; set; }

        [ForeignKey(nameof(Control))]
        public int ControlId { get; set; }

        [ForeignKey(nameof(HazardId))]
        public Hazard Hazard { get; set; }

        [ForeignKey(nameof(ControlId))]
        public Control Control { get; set; }

        [ForeignKey(nameof(Organisation))]
        public int? OrganisationId { get; set; }

        [ForeignKey(nameof(OrganisationId))]
        public Organisation Organisation { get; set; }
    }
}

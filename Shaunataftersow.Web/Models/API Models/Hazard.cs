﻿using System.Collections.Generic;

namespace Shaunataftersow.Models
{
    public class Hazard:GenericItem
    {
        public List<TaskHazard> Tasks { get; set; }=new List<TaskHazard>();

        public List<HazardControl> Controls { get; set; } = new List<HazardControl>();
    }
}

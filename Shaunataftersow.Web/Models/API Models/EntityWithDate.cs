﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Shaunataftersow.Models
{

   public abstract class EntityWithDate :  IEntityWithDate
    {
        [Key]
         public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        protected EntityWithDate()
        {
            CreatedAt=DateTime.Now;
            UpdatedAt=DateTime.Now;
        }

        public DateTime UpdatedAt { get; set; }
    }
}

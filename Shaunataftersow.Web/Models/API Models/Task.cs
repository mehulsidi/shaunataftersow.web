﻿using System.Collections.Generic;

namespace Shaunataftersow.Models
{
    public class Task:GenericItem
    {
        public List<ActivityTask> Activities { get; set; } = new List<ActivityTask>();

        public List<TaskHazard> Hazards { get; set; } = new List<TaskHazard>();
    }
}

﻿using System.ComponentModel.DataAnnotations;
namespace Shaunataftersow.Models

{
    public class Organisation:EntityWithDate
    {
        [StringLength(255)]
        public string Description { get; set; }
    }
}

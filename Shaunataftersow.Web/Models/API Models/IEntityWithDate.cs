﻿using System;

namespace Shaunataftersow.Models
{
    public interface IEntityWithDate
    {
        int Id { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }
}
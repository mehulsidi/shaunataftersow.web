﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Models
{
    public class OrganizationModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Description name is required.")]
        [Display(Name = "Description")]
        public string description { get; set; }
    }
}